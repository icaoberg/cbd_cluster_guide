#!/bin/bash

virtualenv --system-site-packages .
source ./bin/activate
pip install sphinx numpy scipy tabulate matplotlib
deactivate
